const express = require("express");
const Parking = require("../models/Parking");

const router = new express.Router();

router.get("/spaces", async (req, res) => {
  await Parking.find({}).then((p) => {
    return res.status(200).json(p[0].spaces);
  });
});

module.exports = router;
