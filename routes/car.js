const express = require("express");
const Car = require("../models/Car");
const Parking = require("../models/Parking");

const router = new express.Router();

function calculateTax(vehicle) {
  let tax = 0;
  const today = new Date();
  const currentTime = today.getTime();
  const currentTimeFormatted = currentTime.toLocaleTimeString("en-US");

  const day = "8:00:00 AM";
  const night = "6:00:00 PM";

  switch (vehicle.type) {
    case "A":
      if (currentTimeFormatted >= day && currentTimeFormatted <= night) {
        tax += 3;
      } else {
        tax += 2;
      }
      break;
    case "B":
      if (currentTimeFormatted >= day && currentTimeFormatted <= night) {
        tax += 6;
      } else {
        tax += 4;
      }
      break;
    case "C":
      if (currentTimeFormatted >= day && currentTimeFormatted <= night) {
        tax += 12;
      } else {
        tax += 8;
      }
      break;

    default:
      break;
  }

  switch (vehicle.card) {
    case "silver":
      tax += tax * 0.1;
      break;
    case "gold":
      tax += tax * 0.15;
      break;
    case "platinum":
      tax += tax * 0.2;
      break;

    default:
      break;
  }

  return tax;
}

router.get("/tax-check:id", async (req, res) => {
  const id = req.params.id;
  const plateNumber = req.body.plateNumber;
  await Car.find((v) => v.plateNumber === plateNumber).then((car) => {
    if (!car) {
      return res.status(404).json({
        success: false,
        message: "Vehicle is not found!",
      });
    }

    let response = {
      id: id,
      plateNumber: car.plateNumber,
      type: car.type,
      card: car.card,
      date: car.date,
    };

    res.status(200).json(calculateTax(response));
  });
});

router.post("/register-vehicle", async (req, res) => {
  const parking = await Parking.find({}).then((spaces) => {
    return res.status(200).json(spaces[0]);
  });

  let spaces = parking.spaces;

  if (spaces - 1 <= 0 || spaces - 2 <= 0 || spaces - 4 <= 0 || spaces <= 0) {
    return;
  }

  switch (carObj.type) {
    case "A":
      spaces -= 1;
      break;
    case "B":
      spaces -= 2;
      break;
    case "C":
      spaces -= 4;
      break;

    default:
      break;
  }

  await Parking.findByIdAndUpdate(parking._id, { spaces: spaces }).then(() => {
    return res.status(200).json({
      success: true,
      message: "Spaces updated successfully!",
    });
  });

  const carObj = req.body;

  await Car.create(carObj)
    .then(() => {
      res.status(200).json({
        success: true,
        message: "Vehicle registered successfully.",
        data: carObj,
      });
    })
    .catch((err) => {
      console.log(err);
    });
});

router.delete("/unregister-vehicle/:id", async (req, res) => {
  await Car.deleteOne(req.body).then(() => {
    return res.status(200).json({
      success: true,
      message: "Car unregistered successfully!",
    });
  });

  const parking = await Parking.find({}).then((spaces) => {
    return res.status(200).json(spaces[0]);
  });

  let spaces = parking.spaces;

  if (
    spaces + 1 >= 200 ||
    spspaces + 2 >= 200 ||
    spaces + 4 >= 200 ||
    spaces >= 200
  ) {
    return;
  }

  switch (carObj.type) {
    case "A":
      spaces += 1;
      break;
    case "B":
      spaces += 2;
      break;
    case "C":
      spaces += 4;
      break;

    default:
      break;
  }

  await Parking.findByIdAndUpdate(parking._id, { spaces: spaces }).then(() => {
    return res.status(200).json({
      success: true,
      message: "Spaces updated successfully!",
    });
  });
});

module.exports = router;
