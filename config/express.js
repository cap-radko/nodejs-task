const cors = require("cors");
const bodyParser = require("body-parser");
const parkingRoutes = require("../routes/parking");
const carRoutes = require("../routes/car");

module.exports = (app) => {
  app.use(
    bodyParser.urlencoded({
      extended: false,
    })
  );
  app.use(bodyParser.json());
  app.use(
    cors({
      origin: "http://localhost:4200",
    })
  );

  // routes
  app.use("/parking", parkingRoutes);
  app.use("/car", carRoutes);
};
