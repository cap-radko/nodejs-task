const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const Parking = require('../models/Parking');

module.exports = (config) => {
  mongoose.connect(config.dbPath, {
    useNewUrlParser: true,
  });

  const db = mongoose.connection;

  db.once('open', err => {
    if (err) throw err;
    Parking.seedSpaces().then(() => {
      console.log('Database ready');
    }).catch((error) => {
      console.log('Something went wrong');
      console.log(error);
    });
  });

  db.on("error", (reason) => {
    console.log(reason);
  });
};
