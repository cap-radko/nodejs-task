const mongoose = require("mongoose");

const carSchema = new mongoose.Schema({
  plateNumber: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  type: {
    type: mongoose.Schema.Types.String,
    enum: ["A", "B", "C"],
    default: "A",
    required: true
  },
  card: {
    type: mongoose.Schema.Types.String,
    enum: ["common", "silver", "gold", "platinum"],
    default: "common",
  },
  date: {
    type: mongoose.Schema.Types.Date,
    default: new Date(),
  },
});

const Car = mongoose.model("Car", carSchema);

module.exports = Car;
