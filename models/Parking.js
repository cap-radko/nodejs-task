const mongoose = require("mongoose");

const parkingSchema = new mongoose.Schema({
  spaces: {
    type: mongoose.Schema.Types.Number,
    default: 200,
  },
});

const Parking = mongoose.model("Parking", parkingSchema);

Parking.seedSpaces = async () => {
  try {
    const parking = await Parking.find();

    if (parking.length > 0) return;

    Parking.create({
      spaces: 200
    });
  } catch (error) {
    console.log(error);
  }
}

module.exports = Parking;
